<?php
declare(strict_types=1);

namespace Drus\StoreLocator\Controller;

use Drus\StoreLocator\Api\StoreLocatorRepositoryInterface as StoreLocatorRepository;
use Drus\StoreLocator\Model\CheckUrl;
use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;

class Router implements RouterInterface
{
    /**
     * @param ActionFactory $actionFactory
     * @param StoreLocatorRepository $storeRepository
     * @param CheckUrl $url
     */
    public function __construct(
        ActionFactory $actionFactory,
        StoreLocatorRepository $storeRepository,
        CheckUrl $url
    ) {
        $this->actionFactory = $actionFactory;
        $this->storeRepository = $storeRepository;
        $this->url = $url;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        $urlKey = trim($request->getPathInfo(), '/');
        $url = explode('/', $urlKey);

        if (str_contains($urlKey, 'stores')) {
            $request->setModuleName('stores');
            $request->setControllerName('store');
            $request->setActionName('index');
            if (isset($url[1])) {
                $storeUrl = $this->url->checkUrlKeys($url[1]);
                if ($storeUrl) {
                    $store = $this->storeRepository->getById((int)$storeUrl);
                    $request->setParams([
                        'store' => $store
                    ]);
                    return $this->actionFactory->create(Forward::class);
                }
            }
        }
        return null;
    }
}
