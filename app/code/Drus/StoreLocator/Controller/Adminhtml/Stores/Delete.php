<?php

declare(strict_types=1);

namespace Drus\StoreLocator\Controller\Adminhtml\Stores;

use Drus\StoreLocator\Model\Authorization;
use Drus\StoreLocator\Api\Data\StoreLocatorModelInterfaceFactory as StoreLocatorFactory;
use Drus\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;
use Magento\Framework\Controller\ResultInterface;
use Magento\Backend\App\Action\Context;

class Delete extends \Magento\Backend\App\Action implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    public const ADMIN_RESOURCE = Authorization::ACTION_STORE_LOCATOR_DELETE;
    /**
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param StoreLocatorResource $storeLocatorResource
     * @param Context $context
     */
    public function __construct(
        StoreLocatorFactory $storeLocatorFactory,
        StoreLocatorResource $storeLocatorResource,
        Context $context
    ) {
        parent::__construct($context);
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->storeLocatorResource = $storeLocatorResource;
    }
    /**
     * Delete
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($entityID = (int) $this->getRequest()->getParam('entity_id')) {
            try {
                $storeLocator = $this->storeLocatorFactory->create();
                $storeLocator->setId($entityID);
                $this->storeLocatorResource->delete($storeLocator);
                $this->messageManager->addSuccessMessage(__('Store deleted!'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        } else {
            $this->messageManager->addErrorMessage(__('We can\'t find a store to delete.'));
        }

        return $resultRedirect->setPath('*/*/');
    }
}
