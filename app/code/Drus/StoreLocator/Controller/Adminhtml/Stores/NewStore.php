<?php

declare(strict_types=1);

namespace Drus\StoreLocator\Controller\Adminhtml\Stores;

use Drus\StoreLocator\Model\Authorization;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Controller\ResultInterface;

class NewStore extends \Magento\Backend\App\Action implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    public const ADMIN_RESOURCE = Authorization::ACTION_STORE_LOCATOR_EDIT;
    /**
     * @param ForwardFactory $forwardFactory
     * @param Context $context
     */
    public function __construct(
        ForwardFactory $forwardFactory,
        Context $context
    ) {
        parent::__construct($context);
        $this->forwardFactory = $forwardFactory;
    }

    /**
     * Create new store
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        return $this->forwardFactory->create()->forward('edit');
    }
}
