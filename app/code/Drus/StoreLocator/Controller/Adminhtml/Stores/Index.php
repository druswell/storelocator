<?php
declare(strict_types=1);

namespace Drus\StoreLocator\Controller\Adminhtml\Stores;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Index extends \Magento\Backend\App\Action implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    public const ADMIN_RESOURCE = 'Drus_StoreLocator::listing';

    /**
     * @inheritDoc
     * @throws \JsonException
     */
    public function execute():ResultInterface
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Drus_Core::general')->
        getConfig()->getTitle()->prepend(__('Store Locator'));

        return $resultPage;
    }
}
