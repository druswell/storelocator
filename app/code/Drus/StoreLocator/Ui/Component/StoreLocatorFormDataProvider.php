<?php
declare(strict_types=1);
namespace Drus\StoreLocator\Ui\Component;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Drus\StoreLocator\Model\ResourceModel\StoreLocator\Collection;
use Drus\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory;

class StoreLocatorFormDataProvider extends AbstractDataProvider
{
    /**
     * @var Collection $collection
     */
    protected $collection;

    /**
     * @var array $loadedData
     */
    protected array $loadedData;

    /**
     * @var StoreManagerInterface $storeManager
     */
    protected $storeManager;

    /**
     * @param string $name
     * @param StoreManagerInterface $storeManager
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $storeLocatorCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        StoreManagerInterface $storeManager,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $storeLocatorCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection =$storeLocatorCollectionFactory->create();
        $this->storeManager=$storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function getData(): array
    {
        $baseurl =  $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $this->loadedData = [];
        if (!empty($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $store) {
            $temp = $store->getData();
            $img = [];
            $img[0]['name'] = $temp['image'];
            $img[0]['url'] = $baseurl . 'store/image/' . $temp['image'];
            $temp['image'] = $img;
            $this->loadedData[$store->getId()] = array_merge($store->getData(), $temp);
        }
        return $this->loadedData;
    }
}
