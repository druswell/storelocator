<?php
declare(strict_types=1);

namespace Drus\StoreLocator\Ui\Component\Listing\Columns;

use Drus\StoreLocator\Model\Authorization;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class BlockActions extends Column
{
    private const URL_PATH_EDIT = 'drus_store_locator/stores/edit';

    private const URL_PATH_DELETE = 'drus_store_locator/stores/delete';

    /**
     * @var UrlInterface $urlBuilder
     */
    private UrlInterface $urlBuilder;

    /**
     * @var Authorization $authorization
     */
    private Authorization $authorization;

    /**
     * Desc
     *
     * @param Authorization $authorization
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        Authorization $authorization,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->authorization = $authorization;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @return void
     * @throws LocalizedException
     */
    public function prepare():void
    {
        parent::prepare();

        $editAllowed = $this->authorization->isAllowed(Authorization::ACTION_STORE_LOCATOR_EDIT);
        $deleteAllowed = $this->authorization->isAllowed(Authorization::ACTION_STORE_LOCATOR_DELETE);

        if (!$editAllowed && !$deleteAllowed) {
            $config = $this->getConfiguration();
            $config['componentDisabled'] = true;
            $this->setData('config', $config);
        }
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        $editAllowed = $this->authorization->isAllowed(Authorization::ACTION_STORE_LOCATOR_EDIT);
        $deleteAllowed = $this->authorization->isAllowed(Authorization::ACTION_STORE_LOCATOR_DELETE);

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['entity_id'])) {
                    if ($editAllowed) {
                        $item[$this->getData('name')]['edit'] = [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_EDIT,
                                [
                                    'entity_id' => $item['entity_id'],
                                ]
                            ),
                            'label' => __('Edit')
                        ];
                    }

                    if ($deleteAllowed) {
                        $item[$this->getData('name')]['delete'] = [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_DELETE,
                                [
                                    'entity_id' => $item['entity_id'],
                                ]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete'),
                                'message' => __('Are you sure you want to delete this request?'),
                            ],
                            'post' => true
                        ];
                    }
                }
            }
        }

        return $dataSource;
    }
}
