<?php
declare(strict_types=1);

namespace Drus\StoreLocator\Model;

use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Backend\App\Action\Context;

class Geolocation
{
    public const PATH_TO_API_KEY= 'drus_store_locator/general/api_key';

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ClientFactory $guzzleFactory
     * @param Context $context
     * @param $location
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \GuzzleHttp\ClientFactory $guzzleFactory,
        Context $context,
        $location = ''
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->location=$location;
        $this->guzzleFactory= $guzzleFactory;
        $this->context= $context;
        $this->messageManager= $context->getMessageManager();
    }

    /**
     * @param string $address
     * @return array
     * @throws \JsonException
     */
    public function getCoordinates(string $address) : array
    {
        try {
            $apiKey = $this->scopeConfig->getValue(self::PATH_TO_API_KEY, ScopeInterface::SCOPE_STORE);
            $http= $this->guzzleFactory->create();
            $response = $http->get('https://maps.googleapis.com/maps/api/geocode/json?address=' .
                    urlencode($address) . '&key=' . $apiKey);
            $geo = json_decode((string)$response->getBody(), false, 512, JSON_THROW_ON_ERROR);
            $this->location = (array)$geo->results[0]->geometry->location;
        } catch (GuzzleException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
        return $this->location;
    }
}
