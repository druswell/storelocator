<?php
declare(strict_types=1);

namespace Drus\StoreLocator\Model\ResourceModel\StoreLocator;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct():void
    {
        parent::_construct();
        $this->_init(
            \Drus\StoreLocator\Model\StoreLocator::class,
            \Drus\StoreLocator\Model\ResourceModel\StoreLocator::class
        );
    }

}
