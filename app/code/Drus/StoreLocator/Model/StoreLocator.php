<?php
declare(strict_types=1);

namespace Drus\StoreLocator\Model;

use Drus\StoreLocator\Api\Data\StoreLocatorModelInterface;
use Magento\Framework\Model\AbstractModel;

class StoreLocator extends AbstractModel implements StoreLocatorModelInterface
{

    /**
     * @var string
     */
    protected $_eventPrefix = 'store_locator';

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init(\Drus\StoreLocator\Model\ResourceModel\StoreLocator::class);
    }

    /**
     * @return int|string|null
     */
    public function getEntityId(): int|string|null
    {
        return $this->getData(StoreLocatorModelInterface::ENTITY_ID);
    }

    /**
     * @param int $entityId
     * @return $this|StoreLocator
     */
    public function setEntityId($entityId):StoreLocatorModelInterface
    {
        $this->setData(StoreLocatorModelInterface::ENTITY_ID, $entityId);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStoreName():string|null
    {
        return $this->getData(StoreLocatorModelInterface::STORE_NAME);
    }

    /**
     * @param string $storeName
     * @return $this|StoreLocator
     */
    public function setStoreName(string $storeName):StoreLocatorModelInterface
    {
        $this->setData(StoreLocatorModelInterface::STORE_NAME, $storeName);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription():string|null
    {
        return $this->getData(StoreLocatorModelInterface::DESCRIPTION);
    }

    /**
     * @param string $description
     * @return $this|StoreLocator
     */
    public function setDescription(string $description):StoreLocatorModelInterface
    {
        $this->setData(StoreLocatorModelInterface::DESCRIPTION, $description);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage():string|null
    {
        return $this->getData(StoreLocatorModelInterface::IMAGE);
    }

    /**
     * @param string $image
     * @return $this|StoreLocator
     */
    public function setImage(string $image):StoreLocatorModelInterface
    {
        $this->setData(StoreLocatorModelInterface::IMAGE, $image);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress():string|null
    {
        return $this->getData(StoreLocatorModelInterface::ADDRESS);
    }

    /**
     * @param string $address
     * @return $this|StoreLocator
     */
    public function setAddress(string $address):StoreLocatorModelInterface
    {
        $this->setData(StoreLocatorModelInterface::ADDRESS, $address);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSchedule():string|null
    {
        return $this->getData(StoreLocatorModelInterface::SCHEDULE);
    }

    /**
     * @param string $schedule
     * @return $this|StoreLocator
     */
    public function setSchedule(string $schedule):StoreLocatorModelInterface
    {
        $this->setData(StoreLocatorModelInterface::SCHEDULE, $schedule);
        return $this;
    }

    /**
     * @return float|string|null
     */
    public function getLongitude():float|string|null
    {
        return $this->getData(StoreLocatorModelInterface::LONGITUDE);
    }

    /**
     * @param string $longitude
     * @return $this|StoreLocator
     */
    public function setLongitude($longitude):StoreLocatorModelInterface
    {
        $this->setData(StoreLocatorModelInterface::LONGITUDE, $longitude);
        return $this;
    }

    /**
     * @return float|string|null
     */
    public function getLatitude():float|string|null
    {
        return $this->getData(StoreLocatorModelInterface::LATITUDE);
    }

    /**
     * @param string $latitude
     * @return $this|StoreLocator
     */
    public function setLatitude($latitude):StoreLocatorModelInterface
    {
        $this->setData(StoreLocatorModelInterface::LATITUDE, $latitude);
        return $this;
    }
    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->getData(self::STORE_URL_KEY);
    }
    /**
     * @param string $store_url_key
     * @return StoreLocator
     */
    public function setUrl(string $store_url_key): StoreLocator
    {
        $this->setData(self::STORE_URL_KEY, $store_url_key);
        return $this;
    }

}
