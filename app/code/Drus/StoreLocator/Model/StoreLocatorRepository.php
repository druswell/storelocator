<?php
namespace Drus\StoreLocator\Model;

use Drus\StoreLocator\Api\Data\StoreLocatorModelInterface;
use Drus\StoreLocator\Api\Data\StoreLocatorSearchResultInterface;
use Drus\StoreLocator\Api\StoreLocatorRepositoryInterface;
use Drus\StoreLocator\Api\Data\StoreLocatorSearchResultInterfaceFactory;
use Drus\StoreLocator\Model\ResourceModel\StoreLocator as StoreLocatorResource;
use Drus\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory as StoreLocatorCollectionFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Event\ManagerInterface as EventManager;

/**
 * Class PostRepository
 * @package Drus/StoreLocator/Model
 */
class StoreLocatorRepository implements StoreLocatorRepositoryInterface
{
    /**
     * @var array
     */
    private array $registry = [];

    /**
     * @param StoreLocatorResource $storeLocatorResource
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param StoreLocatorCollectionFactory $collectionFactory
     * @param StoreLocatorSearchResultInterfaceFactory $searchResultInterfaceFactory
     */
    public function __construct(
        StoreLocatorResource $storeLocatorResource,
        StoreLocatorFactory $storeLocatorFactory,
        StoreLocatorCollectionFactory $collectionFactory,
        StoreLocatorSearchResultInterfaceFactory $searchResultInterfaceFactory
    ) {
        $this->storeLocatorResource = $storeLocatorResource;
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultInterfaceFactory = $searchResultInterfaceFactory;
    }

    /**
     * @param int $id
     * @return StoreLocatorModelInterface
     * @throws NoSuchEntityException
     */
    public function get(int $id):StoreLocatorModelInterface
    {
        if (!array_key_exists($id, $this->registry)) {
            $storeLocator = $this->storeLocatorFactory->create();
            $this->storeLocatorResource->load($storeLocator, $id);
            if (!$storeLocator->getId()) {
                throw new NoSuchEntityException(__('Requested store does not exist'));
            }
            $this->registry[$id] = $storeLocator;
        }

        return $this->registry[$id];
    }
    /**
     * @param int $store_id
     * @return string|StoreLocatorModelInterface
     */
    public function getById(int $store_id): StoreLocatorModelInterface
    {
        $store = $this->storeLocatorFactory->create();
        $this->storeLocatorResource->load($store, $store_id);
        return $store;
    }


    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return StoreLocatorSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria):StoreLocatorSearchResultInterface
    {
        $collection = $this->collectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        $searchResult = $this->searchResultInterfaceFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($items);
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }

    /**
     * @param StoreLocatorModelInterface $storeLocator
     * @return StoreLocatorModelInterface
     * @throws StateException
     */
    public function save(StoreLocatorModelInterface $storeLocator):StoreLocatorModelInterface
    {
        try {
            /** @var StoreLocator $storeLocator */
            $this->storeLocatorResource->save($storeLocator);
            $this->registry[$storeLocator->getId()] = $this->get($storeLocator->getId());
        } catch (\Exception $exception) {
            throw new StateException(__('Unable to save store #%1', $storeLocator->getId()));
        }
        return $this->registry[$storeLocator->getId()];
    }

    /**
     * @param StoreLocatorModelInterface $storeLocator
     * @return bool
     * @throws StateException
     */
    public function delete(StoreLocatorModelInterface $storeLocator):bool
    {
        try {
            /** @var \Drus\StoreLocator\Model\StoreLocator $storeLocator */
            $this->storeLocatorResource->delete($storeLocator);
            unset($this->registry[$storeLocator->getId()]);
        } catch (\Exception $e) {
            throw new StateException(__('Unable to remove store #%1', $storeLocator->getId()));
        }

        return true;
    }

    /**
     * @param int $id
     * @return bool
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function deleteById(int $id):bool
    {
        return $this->delete($this->get($id));
    }
}
