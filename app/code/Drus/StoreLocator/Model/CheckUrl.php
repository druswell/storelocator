<?php
declare(strict_types=1);

namespace Drus\StoreLocator\Model;

use Drus\StoreLocator\Model\ResourceModel\StoreLocator\Collection;

class CheckUrl
{
    /**
     * @param Collection $collection
     */
    public function __construct(
        Collection $collection
    ) {
        $this->collection = $collection;
    }

    /**
     * @param string $url
     * @return string|bool
     */
    public function checkUrlKeys(string $url): string|bool
    {
        $data = $this->collection->load()->getData();
        foreach ($data as $item) {
            if ($item['store_url_key'] === $url) {
                return $item['entity_id'];
            }
        }
        return false;
    }

    /**
     * @param $url
     * @return bool
     */
    public function checkUniqueUrl($url): bool
    {
        $data = $this->collection->load()->getData();
        foreach ($data as $item) {
            if ($item['store_url_key'] === $url) {
                return false;
            }
        }
        return true;
    }
}
