<?php
declare(strict_types=1);

namespace Drus\StoreLocator\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Drus\StoreLocator\Model\CheckUrl;

class UrlRewrites implements ObserverInterface
{
    /**
     * @param CheckUrl $url
     */
    public function __construct(
        CheckUrl $url
    ) {
        $this->url = $url;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $store = $observer->getData('object');
        if (!empty($store['store_url_key'])) {
            if (!$this->url->checkUniqueUrl($store['store_url_key'])) {
                $this->generatorUrl($store);
            }
        } else {
            $this->generatorUrl($store);
        }
    }
    /**
     * @param $store
     * @return void
     * @throws \Exception
     */
    public function generatorUrl($store): void
    {
        $name = str_replace(' ', '-', strtolower($store['store_name']));
        $generatedUrl = $name . '-' . 'store';
        if ($this->url->checkUniqueUrl($name)) {
            $store->setUrl($name);
        } else {
            $i = 0;
            while (!$this->url->checkUniqueUrl($generatedUrl)) {
                $i++;
                $generatedUrl .= $i;
            }
            $store->setUrl($generatedUrl);
        }
    }
}
