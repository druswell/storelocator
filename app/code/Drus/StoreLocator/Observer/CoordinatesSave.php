<?php

namespace Drus\StoreLocator\Observer;

use Drus\StoreLocator\Model\Geolocation;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CoordinatesSave implements ObserverInterface
{
    /**
     * @param Geolocation $geolocation
     */
    public function __construct(
        Geolocation $geolocation
    ) {
        $this->geolocation = $geolocation;
    }


    /**
     * @param Observer $observer
     * @return void
     * @throws \JsonException
     */
    public function execute(Observer $observer)
    {
        $store = $observer->getData('object');
        $data = $store->getData();
        $coordinates = $this->geolocation->getCoordinates($data['address']);
        $store->setLatitude((string)$coordinates['lat']);
        $store->setLongitude((string)$coordinates['lng']);
    }
}
