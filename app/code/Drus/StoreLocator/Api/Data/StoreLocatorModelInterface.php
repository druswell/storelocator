<?php

namespace Drus\StoreLocator\Api\Data;

use Drus\StoreLocator\Model\StoreLocator;

interface StoreLocatorModelInterface
{
    /**#@+
     * Constants
     * @var string
     */
    public const ENTITY_ID = 'entity_id';
    public const STORE_NAME = 'store_name';
    public const DESCRIPTION ='description';
    public const IMAGE = 'image';
    public const ADDRESS = 'address';
    public const SCHEDULE ='schedule';
    public const LONGITUDE ='longitude';
    public const LATITUDE ='latitude';
    public const STORE_URL_KEY = 'store_url_key';
    /**#@-*/

    /**
     *
     * @return int|string|null
     */
    public function getEntityId():int|string|null;

    /**
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId(int $entityId);

    /**
     * @return string|null
     */
    public function getStoreName():string|null;

    /**
     * @param string $storeName
     * @return $this
     */
    public function setStoreName(string $storeName);

    /**
     * @return string|null
     */
    public function getDescription():string|null;

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description);

    /**
     * @return string|null
     */
    public function getImage():string|null;

    /**
     * @param string $image
     * @return $this
     */
    public function setImage(string $image);

    /**
     * @return string|null
     */
    public function getAddress():string|null;

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress(string $address);

    /**
     * @return string|null
     */
    public function getSchedule():string|null;

    /**
     * @param string $schedule
     * @return $this
     */
    public function setSchedule(string $schedule);

    /**
     * @return float|string|null
     */
    public function getLongitude():float|string|null;

    /**
     * @param string $longitude
     * @return $this
     */
    public function setLongitude($longitude);

    /**
     * @return float|string|null
     */
    public function getLatitude():float|string|null;

    /**
     * @param string $latitude
     * @return $this
     */
    public function setLatitude($latitude);

    /**
     * @return string|null
     */
    public function getUrl(): string|null;

    /**
     * @param string $store_url_key
     * @return StoreLocator
     */
    public function setUrl(string $store_url_key): StoreLocator;

}
