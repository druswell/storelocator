<?php
namespace Drus\StoreLocator\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface StoreLocatorSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Drus\StoreLocator\Api\Data\StoreLocatorModelInterface[]
     */
    public function getItems():StoreLocatorModelInterface;

    /**
     * @param \Drus\StoreLocator\Api\Data\StoreLocatorModelInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
