<?php

namespace Drus\StoreLocator\Plugin;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;


class UrlRewrite
{
    public function beforeSave(\Drus\StoreLocator\Model\ResourceModel\StoreLocator $subject, \Drus\StoreLocator\Model\StoreLocator $model)
    {
        return [$model];
    }
}
