<?php
declare(strict_types=1);

namespace Drus\RegularCustomer\Controller\Adminhtml\Loyalty;

use Drus\RegularCustomer\Model\Authorization;
use Drus\RegularCustomer\Model\RegularCustomerRequestFactory;
use Drus\RegularCustomer\Model\ResourceModel\RegularCustomerRequest;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Edit extends Action implements HttpGetActionInterface
{
    public const ADMIN_RESOURCE = Authorization::ACTION_REGULAR_CUSTOMER_REQUEST_EDIT;

    /**
     * @var RegularCustomerRequestFactory $regularCustomerRequestFactory
     */
    private \Drus\RegularCustomer\Model\RegularCustomerRequestFactory $regularCustomerRequestFactory;

    /**
     *
     * @var RegularCustomerRequest $regularCustomerResource
     */
    private RegularCustomerRequest $regularCustomerResource;

    /**
     * @param RegularCustomerRequestFactory $regularCustomerRequestFactory
     * @param RegularCustomerRequest $regularCustomerResource
     * @param Context $context
     */
    public function __construct(
        RegularCustomerRequestFactory $regularCustomerRequestFactory,
        RegularCustomerRequest $regularCustomerResource,
        Context $context
    ) {
        parent::__construct($context);
        $this->regularCustomerRequestFactory = $regularCustomerRequestFactory;
        $this->regularCustomerResource= $regularCustomerResource;
    }

    /**
     * @return ResultInterface
     */
    public function execute() : \Magento\Framework\Controller\ResultInterface
    {
        $regularCustomerRequest = $this->regularCustomerRequestFactory->create();

        if ($requestId = (int) $this->getRequest()->getParam('request_id')) {
            $this->regularCustomerResource->load($regularCustomerRequest, $requestId);

            if (!$regularCustomerRequest->getId()) {
                $this->messageManager->addErrorMessage(__('This request no exists'));

                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(
            $regularCustomerRequest->getId() ? __('Edit Regular Customer Request') : __('New Regular Customer Request')
        );

        return $resultPage;
    }
}
