<?php
declare(strict_types=1);

namespace Drus\RegularCustomer\Controller\Adminhtml\Loyalty;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Index extends \Magento\Backend\App\Action implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    public const ADMIN_RESOURCE = 'Drus_RegularCustomer::listing';
    /**
     * @inheritDoc
     */
    public function execute():ResultInterface
    {
        $resultpage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultpage->setActiveMenu('Drus_Core::general')->
        getConfig()->getTitle()->prepend(__('Regular Customer'));

        return $resultpage;
    }
}
