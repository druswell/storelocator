<?php

declare(strict_types=1);

namespace Drus\RegularCustomer\Controller\Index;

use Drus\RegularCustomer\Model\RegularCustomerRequest;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;

class Request implements
    \Magento\Framework\App\Action\HttpPostActionInterface,
    \Magento\Framework\App\CsrfAwareActionInterface
{
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    private \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private \Magento\Framework\Message\ManagerInterface $messageManager;

    /**
     * @var \Drus\RegularCustomer\Model\RegularCustomerRequestFactory $regularCustomerRequestFactory
     */
    private \Drus\RegularCustomer\Model\RegularCustomerRequestFactory $regularCustomerRequestFactory;

    /**
     * @var \Drus\RegularCustomer\Model\ResourceModel\RegularCustomerRequest $regularCustomerRequestResource
     */
    private \Drus\RegularCustomer\Model\ResourceModel\RegularCustomerRequest $regularCustomerRequestResource;

    /**
     * @var RequestInterface
     */
    private RequestInterface $request;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private \Magento\Store\Model\StoreManagerInterface $storeManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private \Psr\Log\LoggerInterface $logger;


    /**
     * @param \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Drus\RegularCustomer\Model\RegularCustomerRequestFactory $regularCustomerRequestFactory ,
     * @param \Drus\RegularCustomer\Model\ResourceModel\RegularCustomerRequest $regularCustomerRequestResource
     * @param RequestInterface $request
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Drus\RegularCustomer\Model\RegularCustomerRequestFactory $regularCustomerRequestFactory,
        \Drus\RegularCustomer\Model\ResourceModel\RegularCustomerRequest $regularCustomerRequestResource,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->redirectFactory = $redirectFactory;
        $this->messageManager = $messageManager;
        $this->regularCustomerRequestFactory = $regularCustomerRequestFactory;
        $this->regularCustomerRequestResource = $regularCustomerRequestResource;
        $this->request = $request;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
    }

    /**
     * Controller action
     *
     * @return Redirect
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function execute(): Redirect
    {
        /**
         * @var RegularCustomerRequest $request
         */
        try {
            $request= $this->regularCustomerRequestFactory->create();
            $request->setProductId((int)$this->request->getParam('product_id'))
                ->setName($this->request->getParam('name'))
                ->setEmail($this->request->getParam('email'))
                ->setStoreId($this->storeManager->getStore()->getId());

            $this->regularCustomerRequestResource->save($request);
            $this->messageManager->addSuccessMessage(
                __('%1, your request for loyalty program accepted for review!', $this->request->getParam('name'))
            );
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
            $this->messageManager->addErrorMessage(
                __('Your request can\'t be sent. Please contact us if you see this message')
            );
        }

        $redirect = $this->redirectFactory->create();
        $redirect->setRefererUrl();

        return $redirect;
    }

    /**
     * Create exception in case CSRF validation failed. Return null if default exception will suffice.
     *
     * @param RequestInterface $request
     * @return InvalidRequestException|null
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * Perform custom request validation. Return null if default validation is needed.
     *
     * @param RequestInterface $request
     * @return bool|null
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return null;
    }
}
