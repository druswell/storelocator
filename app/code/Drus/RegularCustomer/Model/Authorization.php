<?php
declare(strict_types=1);

namespace Drus\RegularCustomer\Model;

use Magento\Framework\AuthorizationInterface;

class Authorization
{
    public const ACTION_REGULAR_CUSTOMER_REQUEST_EDIT = 'Drus_RegularCustomer::edit';

    public const ACTION_REGULAR_CUSTOMER_REQUEST_DELETE = 'Drus_RegularCustomer::delete';

    /**
     * @var AuthorizationInterface
     */
    private AuthorizationInterface $authorization;

    /**
     * @param AuthorizationInterface $authorization
     */
    public function __construct(
        AuthorizationInterface $authorization
    ) {
        $this->authorization= $authorization;
    }

    /**
     * Check if resource is available
     *
     * @param string $aclResource
     * @return bool
     */
    public function isAllowed(string $aclResource) :bool
    {
        return $this->authorization->isAllowed($aclResource);
    }

}
