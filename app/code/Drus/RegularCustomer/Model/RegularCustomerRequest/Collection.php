<?php
declare(strict_types=1);

namespace Drus\RegularCustomer\Model\RegularCustomerRequest;

use Drus\RegularCustomer\Model\RegularCustomerRequest;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct():void
    {
        parent::_construct();
        $this->_init(
            RegularCustomerRequest::class,
            \Drus\RegularCustomer\Model\ResourceModel\RegularCustomerRequest::class
        );
    }

}
