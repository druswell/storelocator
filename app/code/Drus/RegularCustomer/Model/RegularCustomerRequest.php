<?php
declare(strict_types=1);

namespace Drus\RegularCustomer\Model;

use Drus\RegularCustomer\Api\Data\RegularCustomerInterface;
use Magento\Framework\Model\AbstractModel;

class RegularCustomerRequest extends AbstractModel implements RegularCustomerInterface
{
    /**
     * @inheritDoc
     */
    protected function _construct(): void
    {
        parent::_construct();
        $this->_init(\Drus\RegularCustomer\Model\ResourceModel\RegularCustomerRequest::class);
    }

    /**
     * @return int
     */
    public function getRequestId(): int
    {
        return $this->getData(RegularCustomerInterface::REQUEST_ID);
    }

    /**
     * @param int $id
     * @return RegularCustomerRequest|void
     */
    public function setRequestId(int $id)
    {
        $this->setData(RegularCustomerInterface::REQUEST_ID, $id);
    }

    /**
     *
     * @return int
     */
    public function getProductId():int
    {
        return $this->getData(RegularCustomerInterface::PRODUCT_ID);
    }

    /**
     * @param int $productId
     * @return $this|RegularCustomerRequest
     */
    public function setProductId(int $productId)
    {
        $this->setData(RegularCustomerInterface::PRODUCT_ID, $productId);
        return $this;
    }


    /**
     * @return array|int|mixed|null
     */
    public function getCustomerId()
    {
        return $this->getData(RegularCustomerInterface::CUSTOMER_ID);
    }

    /**
     * @param int $customerId
     * @return $this|RegularCustomerRequest
     */
    public function setCustomerId(int $customerId)
    {
        $this->setData(RegularCustomerInterface::CUSTOMER_ID, $customerId);
        return $this;
    }

    /**
     * @return array|mixed|string|null
     */
    public function getName()
    {
        return $this->getData(RegularCustomerInterface::NAME);
    }

    /**
     * @param string $name
     * @return $this|RegularCustomerRequest
     */
    public function setName(string $name)
    {
        $this->setData(RegularCustomerInterface::NAME, $name);
        return $this;
    }

    /**
     * @return string|void
     */
    public function getEmail()
    {
        return $this->getData(RegularCustomerInterface::EMAIL);
    }

    /**
     * @param string $email
     * @return RegularCustomerRequest|void
     */
    public function setEmail(string $email)
    {
        $this->setData(RegularCustomerInterface::EMAIL, $email);
        return $this;
    }

    /**
     * @return int|void
     */
    public function getStoreId()
    {
        return $this->getData(RegularCustomerInterface::STORE_ID);
    }

    /**
     * @param $storeId
     * @return RegularCustomerRequest|void
     */
    public function setStoreId($storeId)
    {
        $this->setData(RegularCustomerInterface::STORE_ID, $storeId);
    }

    /**
     * @return string|void
     */
    public function getCreatedAt()
    {
        return $this->getData(RegularCustomerInterface::CREATED_AT);
    }

    /**
     * @param string $createdAt
     * @return RegularCustomerRequest|void
     */
    public function setCreatedAt(string $createdAt)
    {
        $this->setData(RegularCustomerInterface::CREATED_AT);
        return $this;
    }

    /**
     * @return array|mixed|string|null
     */
    public function getUpdatedAt()
    {
        return $this->getData(RegularCustomerInterface::UPDATED_AT);
    }

    /**
     * @param string $updatedAt
     * @return $this|RegularCustomerRequest
     */
    public function setUpdatedAt(string $updatedAt)
    {
        $this->setData(RegularCustomerInterface::UPDATED_AT);
        return $this;
    }
}
