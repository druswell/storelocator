<?php
declare(strict_types=1);

namespace Drus\RegularCustomer\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class RegularCustomerRequest extends AbstractDb
{

    /**
     *
     * @inheirtDoc
     */
    protected function _construct(): void
    {
        $this->_init('drus_regular_customer_request', 'request_id');
    }
}
