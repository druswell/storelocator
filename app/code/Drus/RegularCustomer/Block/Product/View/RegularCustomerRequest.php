<?php
declare(strict_types=1);

namespace Drus\RegularCustomer\Block\Product\View;

use Magento\Framework\View\Element\Template;

class RegularCustomerRequest extends Template
{
    /**
     * Get cache key information incl. current product ID
     *
     * @return array
     */
    public function getCacheKeyInfo(): array
    {
        return array_merge(parent::getCacheKeyInfo(), ['product_id' => $this->getProduct()->getId()]);
    }

}
