<?php
namespace Drus\RegularCustomer\Api\Data;

use Drus\RegularCustomer\Api\Data\RegularCustomerInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface PostRepositoryInterface
 * @package Drus\Api
 * @api
 */
interface RegularCustomerRepositoryInterface
{
    /**
     * @param int $id
     * @return \Drus\RegularCustomer\Api\Data\RegularCustomerInterface
     */
    public function get(int $id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Drus\RegularCustomer\Api\Data\SearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param \Drus\RegularCustomer\Api\Data\RegularCustomerInterface $regularCustomer
     * @return \Drus\RegularCustomer\Api\Data\RegularCustomerInterface
     */
    public function save(RegularCustomerInterface $regularCustomer);

    /**
     * @param \Drus\RegularCustomer\Api\Data\RegularCustomerInterface $regularCustomer
     * @return bool
     */
    public function delete(RegularCustomerInterface $regularCustomer);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id);
}
