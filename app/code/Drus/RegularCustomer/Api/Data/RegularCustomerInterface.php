<?php

namespace Drus\RegularCustomer\Api\Data;

interface RegularCustomerInterface
{
    /**#@+
     * Constants
     * @var string
     */
    public const REQUEST_ID = 'request_id';
    public const PRODUCT_ID = 'product_id';
    public const CUSTOMER_ID ='customer_id';
    public const NAME = 'name';
    public const EMAIL = 'email';
    public const STORE_ID ='store_id';
    public const CREATED_AT ='created_at';
    public const UPDATED_AT ='updated_at';
    /**#@-*/

    /**
     *
     * @return int
     */
    public function getRequestId(): int;

    /**
     *
     * @param int $id
     * @return $this
     */
    public function setRequestId(int $id);

    /**
     * @return int
     */
    public function getProductId();

    /**
     * @param int $productId
     * @return $this
     */
    public function setProductId(int $productId);

    /**
     * @return int
     */
    public function getCustomerId();

    /**
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId(int $customerId);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name);

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email);

    /**
     * @return int
     */
    public function getStoreId();

    /**
     * @param  $storeId
     * @return $this
     */
    public function setStoreId($storeId);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt(string $createdAt);

    /**
     * @return string
     */
    public function getUpdatedAt();

    /**
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt(string $updatedAt);

}
