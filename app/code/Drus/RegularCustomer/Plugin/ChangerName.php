<?php
declare(strict_types=1);

namespace Drus\RegularCustomer\Plugin;

use Drus\RegularCustomer\Api\Data\RegularCustomerInterface;

class ChangerName
{

    /**
     * @param RegularCustomerInterface $regularCustomer
     * @param $name
     * @return string
     */
    public function afterGetName(RegularCustomerInterface $regularCustomer, $name)
    {
        return $shortedname = substr($name, 1, 1).'.';
    }

}
