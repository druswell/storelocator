<?php
declare(strict_types=1);

namespace Drus\ControllerDemos\Controller\demofolder;

use Magento\Framework\Controller\Result\Raw;

class RawResponse implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    private \Magento\Framework\Controller\Result\RawFactory $rawFactory;

    /**
     * @param \Magento\Framework\Controller\Result\RawFactory $rawFactory
     */
    public function __construct(\Magento\Framework\Controller\Result\RawFactory $rawFactory)
    {
        $this->rawFactory = $rawFactory;
    }

    /**
     * @return Raw
     */
    public function execute(): Raw
    {
        return $this->rawFactory->create()
            ->setHeader('Content-Type', 'text/html')
            ->setContents('<form action="https://google.com">
    <input type="submit" value="Go to Google" />
</form>');
    }
}
