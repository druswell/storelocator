<?php
declare(strict_types=1);

namespace Drus\ControllerDemos\Controller\demofolder;

use http\Message\Body;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;

class JsonResponse implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    private JsonFactory $jsonFactory;

    private RequestInterface $request;

    /**
     * @param JsonFactory $jsonFactory
     * @param RequestInterface $request
     */
    public function __construct(JsonFactory $jsonFactory, RequestInterface $request)
    {
        $this->jsonFactory = $jsonFactory;
        $this->request = $request;
    }

    /**
     * @return Json
     */
    public function execute(): Json
    {
       return $this->jsonFactory->create()
           ->setData([
           'first-param'=> $this->request->getParam('first-param'),
           'second-param'=>$this->request->getParam('second-param')
       ]);
    }
}
?>

