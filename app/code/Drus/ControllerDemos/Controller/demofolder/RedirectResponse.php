<?php
declare(strict_types=1);

namespace Drus\ControllerDemos\Controller\demofolder;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;

class RedirectResponse implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    private RedirectFactory $redirect;

    /**
     * @param RedirectFactory $redirect
     */
    public function __construct(
        RedirectFactory $redirect
    ) {
        $this->redirect = $redirect;
    }

    /**
     * @return Redirect
     */
    public function execute(): Redirect
    {
        return $this->redirect->create()
            ->setUrl('https://www.google.com');
    }
}
