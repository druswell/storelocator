<?php
declare(strict_types=1);

namespace Drus\ControllerDemo\Controller\FolderForClassController;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Forward;

class CategoryForward implements HttpGetActionInterface
{

    private \Magento\Framework\Controller\Result\ForwardFactory $forwardFactory;

    /**
     * @param \Magento\Framework\Controller\Result\ForwardFactory $forwardFactory
     */
    public function __construct(
        \Magento\Framework\Controller\Result\ForwardFactory $forwardFactory
    ) {
        $this->forwardFactory= $forwardFactory;
    }

    /**
     * Controller demo
     *
     * @return Forward
     */
    public function execute():Forward
    {
        return $this->forwardFactory->create()
            ->forward('controllerclass');
    }
}
