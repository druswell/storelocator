<?php
declare(strict_types=1);

namespace Drus\ControllerDemo\Controller\FolderForClassController;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;

class ControllerClass implements HttpGetActionInterface
{
    private \Magento\Framework\App\RequestInterface $request;

    private \Magento\Framework\Controller\Result\JsonFactory $jsonFactory;

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
    ) {
        $this->request = $request;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     *
     * Controller demo
     *
     * @return Json
     */
    public function execute():Json
    {
        return $this->jsonFactory->create()
            ->setData([
                'first-param'=>(int) $this->request->getParam('first-param', 10),
                'second-param'=>$this->request->getParam('second-param')
            ]);
        /*
        echo 'First int: ' . (int) $this->request->getParam('first-param', 10) . '<br>';
        echo 'Second int: ' . $this->request->getParam('second-param');
        echo '<br>testing controller';
        exit;
        */
    }
}
